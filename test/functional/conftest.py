import pytest

from app.app import app as flask_app
from flask_webtest import TestApp


@pytest.fixture(scope="session")
def app(request):
    app = flask_app
    ctx = app.app_context()
    ctx.push()

    def teardown():
        ctx.pop()

    request.addfinalizer(teardown)
    return app


@pytest.fixture(scope="session")
def testapp(app):
    return TestApp(app)
