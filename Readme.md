
## Installation 

* Install python 3.7 & python pip 
* Create virtual env: `python3 -m venv venv`
* Activate virtual environment: `source venv/bin/activate`
* Install requirements: `pip insa`


## 

* Run the test cases : `py.test test`